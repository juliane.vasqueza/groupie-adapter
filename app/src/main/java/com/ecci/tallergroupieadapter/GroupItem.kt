package com.ecci.tallergroupieadapter

import com.ecci.tallergroupieadapter.databinding.ItemGroupBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.databinding.BindableItem

class GroupItem: BindableItem<ItemGroupBinding>(), ExpandableItem{

    lateinit var expandibleGroup: ExpandableGroup

    override fun bind(viewBinding: ItemGroupBinding, position: Int) {
        viewBinding.toggleButton.setOnClickListener {
            expandibleGroup.onToggleExpanded()
            if(expandibleGroup.isExpanded){
                viewBinding.toggleButton.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
            }
            else{
                viewBinding.toggleButton.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
            }
        }
    }

    override fun getLayout(): Int {
        return R.layout.item_group
    }

    override fun setExpandableGroup(onToggleListener: ExpandableGroup) {
        this.expandibleGroup = onToggleListener
    }

}