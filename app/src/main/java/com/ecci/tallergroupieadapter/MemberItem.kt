package com.ecci.tallergroupieadapter

import com.ecci.tallergroupieadapter.databinding.ItemMemberBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.databinding.BindableItem

class MemberItem(val name: String, val code: String): BindableItem<ItemMemberBinding>(){


    override fun bind(viewBinding: ItemMemberBinding, position: Int) {
        viewBinding.nameTextView.text = name
        viewBinding.codeTextView.text = code
    }

    override fun getLayout(): Int {
        return  R.layout.item_member
    }

}