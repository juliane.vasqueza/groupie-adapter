package com.ecci.tallergroupieadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.ecci.tallergroupieadapter.databinding.ActivityMainBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.GroupieAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        binding.groupRecyclerView.layoutManager = LinearLayoutManager(this)

        val section = ExpandableGroup(GroupItem())
        section.add(MemberItem("Sebastian Contreras Gómez", "Código: 54371"))
        section.add(MemberItem("Julian Esteban Vasquez Arias", "Código: 45032"))
        val adapter = GroupieAdapter()
        binding.groupRecyclerView.adapter = adapter
        adapter.add(section)
    }
}
